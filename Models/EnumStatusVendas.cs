namespace ApiTestePottencial.Models
{
    public enum EnumStatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelado
    }
}

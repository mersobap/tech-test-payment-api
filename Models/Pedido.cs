using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiTestePottencial.Models
{
    public class Pedido
    {
        [Key()]
        public int Id { get; set; }
        [ForeignKey("Vendedor")]
        public int VendedorId { get; set; }
        public DateTime Data { get; set; }
        [ForeignKey("Item")]
        public int ItemId { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}
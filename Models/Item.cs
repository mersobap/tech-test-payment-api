namespace ApiTestePottencial.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set; }
    }
}
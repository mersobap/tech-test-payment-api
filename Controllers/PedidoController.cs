using ApiTestePottencial.Context;
using ApiTestePottencial.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiTestePottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PedidoController: Controller
    {
        private readonly VendasContext MeuContexto;
        
        public PedidoController(VendasContext contexto)
        {
            MeuContexto = contexto;
        }

        [HttpGet("BuscarVendaPorId")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = MeuContexto.Pedidos.Find(id);

            if (venda == null)
                return NotFound();

            
            return Ok(venda);
        }
        [HttpPost]
        public IActionResult RegistrarVenda(Pedido pedido)
        {
            var vendedor = MeuContexto.Vendedores.Find(pedido.VendedorId);
            if (vendedor == null)
                return BadRequest(new { Erro = "Vendedor não encontrado!" });

            var item = MeuContexto.Itens.Find(pedido.ItemId);
            if (item == null)
                return BadRequest(new { Erro = "Item não encontrado!" });

            pedido.Status = EnumStatusVenda.Aguardando_Pagamento;
            pedido.Data = DateTime.Now;

            MeuContexto.Add(pedido);
            MeuContexto.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarVenda), new { id = pedido.Id }, pedido);
        }
        [HttpPost("AtualizarPedido")]
        public IActionResult AtualizarPedido(int id, EnumStatusVenda novostatus)
        {
            var pedidoAtual = MeuContexto.Pedidos.Find(id);

            if (pedidoAtual == null)
                return NotFound();
            // Testando as condições de atualização de Status dos pedidos
            if ( pedidoAtual.Status == EnumStatusVenda.Aguardando_Pagamento )
            {
                if ((novostatus == EnumStatusVenda.Pagamento_Aprovado) | (novostatus == EnumStatusVenda.Cancelado))
                    pedidoAtual.Status = novostatus;
                else
                    return BadRequest(new { Erro = "Atualização de pedido não disponível!" });
            }
            else if ( pedidoAtual.Status == EnumStatusVenda.Pagamento_Aprovado )
            {
                if ((novostatus == EnumStatusVenda.Enviado_para_transportadora) | (novostatus == EnumStatusVenda.Cancelado))
                    pedidoAtual.Status = novostatus;
                else
                    return BadRequest(new { Erro = "Transição imprópria!" });
            }
            else if ( pedidoAtual.Status == EnumStatusVenda.Enviado_para_transportadora )
            {
                if (novostatus == EnumStatusVenda.Entregue)
                    pedidoAtual.Status = novostatus;
                else
                    return BadRequest(new { Erro = "Transição imprópria!" });
            }
            else
            {
                return BadRequest(new { Erro = "Transição imprópria!" });
            }

            MeuContexto.Pedidos.Update(pedidoAtual);
            MeuContexto.SaveChanges();

            return Ok(pedidoAtual);
        }
    }
}


using ApiTestePottencial.Context;
using ApiTestePottencial.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiTestePottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController: Controller
    {
        private readonly VendasContext MeuContexto;
        
        public VendedorController(VendasContext contexto)
        {
            MeuContexto = contexto;
        }

        [HttpPost("IncluirVendedor")]
        public IActionResult IncluirVendedor(Vendedor vendedor)
        {
            MeuContexto.Add(vendedor);
            MeuContexto.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarVendedorPorId), new { id = vendedor.Id }, vendedor);  
        }
        [HttpGet("BuscarVendedorPorId")]
        public IActionResult BuscarVendedorPorId(int id)
        {
            var vendedor = MeuContexto.Vendedores.Find(id);

            if(vendedor == null){
                return NotFound();
            }

            return Ok(vendedor);
        }
        [HttpGet("RetornarVendedores")]
        public IActionResult RetornarVendedores()
        {
            var vendedor = MeuContexto.Vendedores;
            return Ok(vendedor);
        }
        [HttpDelete("ExcluirVendedor")]
        public IActionResult Excluir(int id)
        {
            var vendedor = MeuContexto.Vendedores.Find(id);

            if (vendedor == null){
                return NotFound();
            }
            MeuContexto.Vendedores.Remove(vendedor);
            MeuContexto.SaveChanges();
            return NoContent();
        }
        [HttpPut("AtualizarVendedor")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBd = MeuContexto.Vendedores.Find(id);
            
            if (vendedorBd == null){
                return NotFound();
            }

            vendedorBd.Nome = vendedor.Nome;
            vendedorBd.CPF = vendedor.CPF;
            vendedorBd.Email = vendedor.Email;
            vendedorBd.Telefone = vendedor.Telefone;

            MeuContexto.Vendedores.Update(vendedorBd);
            MeuContexto.SaveChanges();
            return Ok(vendedorBd);
        }
    }
    
}
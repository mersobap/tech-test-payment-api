using ApiTestePottencial.Context;
using ApiTestePottencial.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiTestePottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController: Controller
    {
        private readonly VendasContext MeuContexto;
        
        public ItemController(VendasContext contexto)
        {
            MeuContexto = contexto;
        }

        [HttpPost("IncluirItem")]
        public IActionResult IncluirItem(Item item)
        {
            MeuContexto.Add(item);
            MeuContexto.SaveChanges();
            
            return CreatedAtAction(nameof(BuscarItemPorId), new { id = item.Id }, item);  
        }
        [HttpGet("BuscarItemPorId")]
        public IActionResult BuscarItemPorId(int id)
        {
            var item = MeuContexto.Itens.Find(id);

            if(item == null){
                return NotFound();
            }

            return Ok(item);
        }
        [HttpGet("RetornarTodosOsItens")]
        public IActionResult RetornarTodosOsItens()
        {
            var item = MeuContexto.Itens;
            return Ok(item);
        }

        [HttpDelete("ExcluirItem")]
        public IActionResult ExcluirItem(int id)
        {
            var item = MeuContexto.Itens.Find(id);

            if (item == null){
                return NotFound();
            }
            MeuContexto.Itens.Remove(item);
            MeuContexto.SaveChanges();
            return NoContent();
        }

        [HttpPost("AlterarItem")]
        public IActionResult AlterarItem(int id, Item item)
        {
            var itemAtual = MeuContexto.Itens.Find(id);
            
            if (itemAtual == null){
                return NotFound();
            }

            itemAtual.Descricao = item.Descricao;
            itemAtual.Preco = item.Preco;
            
            MeuContexto.Itens.Update(itemAtual);
            MeuContexto.SaveChanges();
            return Ok(itemAtual);
        }
    }
    
}
using Microsoft.EntityFrameworkCore;
using ApiTestePottencial.Models;

namespace ApiTestePottencial.Context
{
    public class VendasContext: DbContext
    {
        public VendasContext(DbContextOptions<VendasContext> options): base(options)
        {

        }

        public DbSet<Pedido> Pedidos {get; set;}
        public DbSet<Item> Itens {get; set;}
        public DbSet<Vendedor> Vendedores {get; set;}
    }
}